using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using NUnit.Framework;

namespace logic
{

	[TestFixture]
	public class Fidonet_Test
	{

		[Test]
		public void Test1()
		{
			File.WriteAllBytes("data", File.ReadAllBytes("pic.bmp").Skip(54).ToArray());
		}

		[Test]
		public void Test()
		{
			Console.WriteLine(new DirectoryInfo(".").FullName);
			var bmp = (Bitmap)Image.FromFile("pic.bmp");
			Fidonet fidonet = new Fidonet(bmp);
			foreach (var source in fidonet.Forward(new P(70, 79), new P(18, 26)).Take(20)) ;
			//62-49 252-4
			P p = new P(54, 57);
			P v = new P(0, 8);
			var clr = 113;
			var back = fidonet.Backward(p, v);
			foreach (var pv in back.Take(100))
			{
				Console.WriteLine(pv);
			}
		}
		
		[Test]
		public void Test2()
		{
			var bmp = (Bitmap)Image.FromFile("pic.bmp");
			var fidonet = new Fidonet(bmp);
			P p = new P(149,116);
			P v = new P(40, 30);
			var ppv = fidonet.Backward(p, v).Last();
			fidonet.OpenPage(ppv.Item1, ppv.Item3);
		}
	}

	public class Fidonet
	{
		private readonly Bitmap bmp;

		public Fidonet(Bitmap bmp)
		{
			this.bmp = bmp;
		}

		public Bitmap OpenPage(P p, P v)
		{
			var segments = Forward(p, v).ToList();
			Console.WriteLine(segments.Count());
			var res = new Bitmap(400, 200);
			Graphics g = Graphics.FromImage(res);
			g.FillRectangle(Brushes.Black, 0, 0, res.Width, res.Height);
			foreach (var s in segments)
			{
				g.DrawLine(Pens.Yellow, s.Item1.X, s.Item1.Y, s.Item2.X, s.Item2.Y);
			}
			g.Dispose();
			string filename = "res-" + p + "-" + v + ".png";
			res.Save(filename);
			Process.Start(filename);
			return res;
		}

		public IEnumerable<Tuple<P, P>> Forward(P p, P v, int clr = 0)
		{
			while (true)
			{
				Color c = bmp.GetPixel(p.X, p.Y);
				if (c.R == 0 && c.G == 0 && c.B == 0) yield break;
				v = new P(v.X ^ c.B, v.Y ^ c.G);
				clr = clr ^ c.R;
				var p0 = p;
				p = new P(p.X + (sbyte)v.X, p.Y + (sbyte)v.Y);
//				Console.WriteLine(p + " " + v + " " + clr);
				if (clr > 0)
				{
					yield return Tuple.Create(p0, p);
				}
			}
		}

		public IEnumerable<Tuple<P, P, P>> Backward(P p, P v)
		{
			int clr = bmp.GetPixel(p.X, p.Y).R;
			while (true)
			{
				P prev = new P(p.X - (sbyte) v.X, p.Y - (sbyte)v.Y);
				Color c = bmp.GetPixel(prev.X, prev.Y);
				if (c.R == 0 && c.G == 0 && c.B == 0) yield break;
				v = new P(v.X ^ c.B, v.Y ^ c.G);
				if (clr > 0)
					yield return Tuple.Create(prev, p, v);
				clr = clr ^ c.R;
				p = prev;
				if (clr==0) yield break;
			}
		}
	}
}