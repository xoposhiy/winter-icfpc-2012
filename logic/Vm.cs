using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Linq;

namespace logic
{
	public class Vm
	{
		private readonly uint[] a = new uint[13371111 + 3];
		private StringBuilder sb = new StringBuilder();
		private HashSet<uint> code;

		public uint[] CopyMemory()
		{
			var res = new uint[a.Length];
			Array.Copy(a, res, a.Length);
			return res;
		}

		public static uint[] Load(string filename = "data")
		{
			var dataBytes = File.ReadAllBytes(filename);
			var data = new List<uint>();
			for (var i = 0; i < dataBytes.Length; i += 4)
			{
				var t = BitConverter.ToUInt32(dataBytes, i);
				data.Add(t);
			}
			return data.ToArray();
		}

		public Vm(uint[] data)
		{
			Array.Copy(data, a, data.Length);
		}

		public uint Ip { get; set; }
		public uint Iterations;

		public string Result { get { return sb.ToString(); } }

		public void Run(uint sip = 32)
		{
			Run(sip, () => false, list => { });
		}

		public void Run(uint sip, Func<bool> isCanceled, Action<IEnumerable<uint>> reportChangedMemory)
		{
			Iterations = 0;
//			var edges = new Dictionary<string, int>();
			code = new HashSet<uint>();
			Ip = sip;
			string prevInstr = null;
			var changed = new HashSet<uint>();
			while (true)
			{
				var prevIp = Ip;
				var t = a[Ip];
				if (t == 0) break;
				Iterations++;
				if (Iterations % 100000 == 0)
				{
					reportChangedMemory(changed);
					changed = new HashSet<uint>();
					if (isCanceled()) return;
				}
				var cmd = GetCmdCode(t);
//				Console.Out.WriteLine("t = {0:X8}, Ip = {1}, Cmd = {2}", t, Ip, cmd);
				var t1 = a[Ip + 1];
				var t2 = a[Ip + 2];
				var t3 = a[Ip + 3];
				var old = a[Ip + t1];
				var offset = GetOffset(t);
//				var instr = Ip + "_" + cmd.ToString();
				switch (cmd)
				{
//					case Cmd.Zero:
//						break;
					case Cmd.Plus:
						a[Ip + t1] = a[Ip + t2] + t3;
						break;
					case Cmd.Minus:
						a[Ip + t1] = a[Ip + t2] - t3;
						break;
					case Cmd.Mult:
						a[Ip + t1] = a[Ip + t2] * t3;
						break;
					case Cmd.Divide:
						a[Ip + t1] = a[Ip + t2] / t3;
						break;
					case Cmd.And:
						a[Ip + t1] = a[Ip + t2] & t3;
						break;
					case Cmd.Or:
						a[Ip + t1] = a[Ip + t2] | t3;
						break;
					case Cmd.Shl:
						a[Ip + t1] = a[Ip + t2] << (int)t3;
						break;
					case Cmd.Shr:
						a[Ip + t1] = a[Ip + t2] >> (int)t3;
						break;
					case Cmd.Not:
						a[Ip + t1] = ~t2;
						break;
					case Cmd.Jl:
						if (t2 < t3)
							Ip += t1;
						else MoveNext(offset);
						break;
					case Cmd.Prn:
						var c = (char) t1;
						sb.Append(c);
						Console.Out.Write(c);
						break;
					default:
						//Console.Out.WriteLine("Bad cmd {0:X8} in {1:X8}", cmd, t);
//						Console.Out.WriteLine(sb);
						sb.Clear();
						return;
				}

				if (cmd != Cmd.Jl)
					MoveNext(offset);

				if (cmd != Cmd.Jl && cmd != Cmd.Prn)
				{
					var adr = (prevIp + t1);
					changed.Add(adr);
				}

//				if (prevInstr != null)
//				{
//					var e = prevInstr + " -> " + instr;
//					if (edges.ContainsKey(e))
//						edges[e]++;
//					else edges[e] = 1;
//				}
//				prevInstr = instr;
//				i++;
//				if (i % 20000000 == 0)
//				{
//					Console.WriteLine(i);
////					SaveMem(m, i);
//					//					File.WriteAllText("graph.gv", "digraph a { " + string.Join("\r\n", edges.Select(kv => kv.Key + "[ weight=" + kv.Value + "]")) + "}");
//				}
			}
//			SaveMem(m, 1234567890);
		}

		private void SaveMem(bool[] m, int i)
		{
			var width = 256;
			Bitmap bmp = new Bitmap(width, 1 + 13371111 / width);
			for (int j = 0; j < m.Length; j++)
			{
				if (m[j])
				{
					var b = BitConverter.GetBytes(a[j]);
					bmp.SetPixel(
						j % width,
						j / width,
						IsCmd(GetCmdCode(a[j]))
						? Color.FromArgb(255, 255, 255, 255)
						: Color.FromArgb(255, b[0], b[1], b[2]));
				}
			}
			bmp.Save("mem_" + i + ".png");
		}

		private void MoveNext(int offset)
		{
			Ip = (uint)(Ip + offset);
		}

		public static bool IsCmd(Cmd cmd)
		{
			return (int)cmd >= 1 && (int)cmd <= 11;
		}

		public static int GetOffset(uint t)
		{
			return (Int16)(t & 0xffff);
		}

		public static Cmd GetCmdCode(uint t)
		{
			return (Cmd)(t >> 16);
		}

		public uint GetMem(uint adr)
		{
			return a[adr];
		}
	}

	public enum Cmd
	{
		Zero = 0,
		Plus = 1,
		Minus = 2,
		Mult = 3,
		Divide = 4,
		And = 5,
		Or = 6,
		Shl = 7,
		Shr = 8,
		Not = 9,
		Jl = 10,
		Prn = 11,
	}
}