namespace logic
{
	public class P
	{
		public P(int x, int y)
		{
			X = x;
			Y = y;
		}

		public override string ToString()
		{
			return string.Format("{0}-{1}", X, Y);
		}

		public readonly int X, Y;
	}
}