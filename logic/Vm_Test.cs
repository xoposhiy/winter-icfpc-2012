using System;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;
using System.Linq;

namespace logic
{
	[TestFixture]
	public class Vm_Test
	{
		[Test]
		public void Test()
		{
			int[] r = new int[12];
			var m = Vm.Load();
			string s = "";
			for (int i = 0; i < m.Length; i++)
			{
				var cmd = Vm.GetCmdCode(m[i]);
				if (Vm.IsCmd(cmd))
					r[(int)cmd]++;
				if (cmd == Cmd.Prn)
				{
					char ch = (char) m[i + 1];
					if (ch < 128) s += ch;
				}
			}
			Console.WriteLine(s);
			for (int i = 1; i <= 11; i++)
			{
				Console.WriteLine(r[i] + " " + (Cmd)i);
			}
		}

		

		[Test]
		public void Bf()
		{
			var m = Vm.Load();
			for (uint i = 0; i < m.Length; i++)
			{
				var cmd = Vm.GetCmdCode(m[i]);
				if (Vm.IsCmd(cmd))
				{
					try
					{
						var vm = new Vm(m);
						vm.Run(i);
					}
					catch (Exception)
					{
						
					}
					
				}
			}
		}

		[Test]
		public void Run32()
		{
			var m = Vm.Load();
			Vm vm32 = new Vm(m);
			vm32.Run();
			Console.WriteLine(vm32.Iterations);
			Console.WriteLine(vm32.Result);
			m = vm32.CopyMemory();
			var bytes = new List<byte>(m.Length * 4);
			foreach (uint word in m)
			{
				bytes.AddRange(BitConverter.GetBytes(word));
			}
			File.WriteAllBytes("32", bytes.ToArray());
		}

		[Test]
		public void Bf2()
		{
			var m = Vm.Load("32");
			for (uint i = 16622*256; i < m.Length; i++)
//			foreach (uint i in new uint[] {3205760, 1049584, 1049568, 1049552, 1049536, 1049520, 1049504, 1049488, 1049472, 1049456, 1049440, 1049424, 1049408, 1049392, 1049376, 1049360, 1049344, 1049328, 1049312, 1049296, 1049280, 1049264, 1049248, 1049232, 1049216, 1049200, 1049184, 1049168, 1049152, 1049136, 1049120, 1049104, 1049088, 1049072, 1049056, 1049040, 1049024, 1049008, 1048992, 1048976, 1048960, 1048944, 1048928, 1048912, 1048896, 1048880, 1048864, 1048848, 1048832, 1048816, 1048800, 1048784, 1048768, 1048752, 1048736, 1048720, 1048704, 1048688, 1048672, 1048656, 1048640, 1048624, 1048608, 1048592, 1048576, 0 })
			{
				if (i % 1000 == 0) Console.WriteLine(DateTime.Now + " " + i);
				var cmd = Vm.GetCmdCode(m[i]);
				if (Vm.IsCmd(cmd))
				{
					try
					{
						var vm = new Vm(m);
						vm.Run(i);
						if (vm.Result.Length > 0 && vm.Iterations > 10)
						{
							Console.WriteLine();
							Console.WriteLine("SIP: {0}", i);
							Console.WriteLine(vm.Iterations + " iterations. Result writen to " + i + ".txt");
							File.WriteAllText(i + ".txt", vm.Result);
						}
					}
					catch (Exception e)
					{
						Console.WriteLine(e.Message);
					}
				}
			}
		}
	}
}