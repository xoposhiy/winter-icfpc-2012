﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using logic;

namespace showmethemoney
{
	public partial class Form1 : Form
	{
		private Bitmap bmp;
		private Vm vm;
		private const int width = 1024;
		private const int height = 1 + 13371111 / width;

		public Form1()
		{
			InitializeComponent();
		}

		private void toolStripButton1_Click(object sender, EventArgs e)
		{
			pictureBox.Width = 1024;
			pictureBox.Height = height;
			bmp = new Bitmap(width, height);
			pictureBox.Image = new Bitmap(width, height);
			using(var g = Graphics.FromImage(bmp))
				g.FillRectangle(Brushes.Black, 0, 0, width, height);
			vm = new Vm(Vm.Load());
			Text = "working...";
			bw.RunWorkerAsync();
		}
		class State
		{
			public uint iterations;
			public Bitmap image;
			public string result;

		}
		private void bw_DoWork(object sender, DoWorkEventArgs e)
		{
			vm.Run(32, () => e.Cancel, changed =>
				{
					Text = "changed: " + changed.Count();
					foreach (var adr in changed)
					{
						var word = vm.GetMem(adr);
						var b = BitConverter.GetBytes(word);
						bmp.SetPixel(
							((int)((adr+24*16) % width)),
							(int)((adr + 24*16) / width),
							Color.FromArgb(255, b[0], b[0], b[0]));
					}
					var b2 = new Bitmap(width, height);
					using (var g = Graphics.FromImage(b2))
					{
						g.DrawImage(bmp, 0, 0);
					}
					bw.ReportProgress(0, new State() { iterations = vm.Iterations, image = b2, result = vm.Result });
				});
		}

		private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			var s = (State) e.UserState;
			pictureBox.Image.Dispose();
			pictureBox.Image = s.image;
			textBox1.Text = s.iterations + "\r\n" + s.result;
		}

		private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			Text = "done!";
		}
	}
}
