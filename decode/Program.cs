﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using logic;

namespace decode
{
	class Program
	{
		static void Main(string[] args)
		{
			var m = Vm.Load();
			var vm = new Vm(m);
			vm.Run();



			/*for (uint sip = 0; sip < m.Length; sip++)
			{
				var cmd = Vm.GetCmdCode(m[sip]);
				if (Vm.IsCmd(cmd))
				{
					try
					{
						
					}
					catch (Exception)
					{
					}
				}
			}*/

			/*var data = Vm.Load();
			var vm = new Vm(data);
			vm.Run();*/
			Console.ReadKey();

			//ShowComments();
			//var bmp = (Bitmap) Image.FromFile("pic.bmp");
			//InspectAlpha(bmp);
			//ShowManPage(args, bmp);
		}

		private static void InspectAlpha(Bitmap bmp)
		{
			for (int x = 0; x < bmp.Width; x++)
			{
				for (int y = 0; y < bmp.Height; y++)
				{
					var a = bmp.GetPixel(x, y).A;
					if (a != 255)
						Console.Out.WriteLine(a);
				}
			}
		}

		private static void ShowManPage(string[] args, Bitmap bmp)
		{
//			var p = new P(70, 79);var v = new P(18, 26);
//			var p = new P(50, 22);var v = new P(24, 34);
//			var p = new P(41, 19);var v = new P(37, 25);
			var p = new P(int.Parse(args[0]), int.Parse(args[1]));
			var v = new P(int.Parse(args[2]), int.Parse(args[3]));
			Console.WriteLine(p);
			Console.WriteLine(v);
			new Fidonet(bmp).OpenPage(p, v);
		}

		private static void ShowComments()
		{
			string text = File.ReadAllText("pic.bmp", Encoding.ASCII);
			MatchCollection matches = Regex.Matches(text, "###[^\r\n]+");
			foreach (var result in matches.Cast<Match>().Select(m => m.Value))
			{
				Console.WriteLine(result);
			}
		}
	}
}
